import { NgParishPage } from './app.po';

describe('ng-parish App', function() {
  let page: NgParishPage;

  beforeEach(() => {
    page = new NgParishPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
