import { Component } from '@angular/core';
import { AuthGuard } from './services/auth-guard.service';
import { AlertService } from './services/alert.service';
import { environment } from '../environments/environment';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public authGuard: AuthGuard, public alertService: AlertService, private translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use(environment.appConfig.language);
  }

  get appTitle(): string {
    return environment.appConfig.title;
  }
  get appSubtitle(): string {
    return environment.appConfig.subtitle;
  }

  get logoFilename(): string {
    return environment.appConfig.logoFilename;
  }

}
