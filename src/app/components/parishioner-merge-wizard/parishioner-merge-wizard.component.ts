import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ParishService } from 'app/services/parish.service';
import { Observable } from 'rxjs/Observable';
import { Parishioner } from 'app/models/parishioner';
import { ConfirmationService } from 'primeng/primeng';
import { AlertService } from 'app/services/alert.service';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-parishioner-merge-wizard',
  templateUrl: './parishioner-merge-wizard.component.html',
  styleUrls: ['./parishioner-merge-wizard.component.css']
})
export class ParishionerMergeWizardComponent implements OnInit {

  @Input() parishioner: Parishioner;

  @Output() parishionerMergeSuccess: EventEmitter<void> = new EventEmitter();

  parishioners: Observable<Parishioner[]>;

  constructor(
    private parishService: ParishService,
    private confirmationService: ConfirmationService,
    private alertService: AlertService,
    private translate: TranslateService) { }

  ngOnInit() {
    this.parishioners = this.parishService.getParishioners().map(p => p.filter(v => v.$key !== this.parishioner.$key));
  }

  confirmMergeWith(sourceParishioner: Parishioner) {
    this.confirmationService.confirm({
      message: this.translate.instant('MergeAlert', { name1: sourceParishioner.name, name2: this.parishioner.name }),
      accept: () => {
        this.parishService.mergeParishioners(this.parishioner, sourceParishioner)
          .subscribe(() => {
            this.alertService.alertTranslated('MergeSuccessAlert');
            this.parishionerMergeSuccess.emit();
          });
      }
    });
  }

}
