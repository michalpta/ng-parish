import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { Parishioner } from 'app/models/parishioner';
import { ParishService } from 'app/services/parish.service';

@Component({
  selector: 'app-parishioner-form',
  templateUrl: './parishioner-form.component.html',
  styleUrls: ['./parishioner-form.component.css']
})
export class ParishionerFormComponent implements OnInit {

  @ViewChild('nameInput') nameInput: ElementRef;

  private _parishioner: Parishioner;
  @Input() set parishioner(value: Parishioner) {
    this._parishioner = value;
    setTimeout(() => this.nameInput.nativeElement.select(), 100);
  };
  get parishioner() {
    return this._parishioner;
  }

  @Output() addSuccess: EventEmitter<any> = new EventEmitter();
  @Output() updateSuccess: EventEmitter<void> = new EventEmitter();
  @Output() cancelClick: EventEmitter<void> = new EventEmitter();

  @Input() hideCancel = false;

  processing = false;

  constructor(private parishService: ParishService) { }

  ngOnInit() {
  }

  submitParishioner() {
    if (!this.parishioner.$key) {
      this.addParishioner();
    } else {
      this.updateParishioner();
    }
  }

  addParishioner() {
    if (!this.parishioner.name) {
      return;
    }
    this.processing = true;
    this.parishService.addParishioner(this.parishioner)
      .then(x => {
        this.addSuccess.emit(x);
        this.processing = false;
      });
  }

  updateParishioner() {
    if (!this.parishioner.name) {
      return;
    }
    this.processing = true;
    this.parishService.updateParishioner(this.parishioner)
      .then(x => {
        this.updateSuccess.emit();
        this.processing = false;
      });
  }

  emitCancel() {
    this.cancelClick.emit();
  }

}
