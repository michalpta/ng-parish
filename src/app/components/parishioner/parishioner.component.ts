import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-parishioner',
  templateUrl: './parishioner.component.html',
  styleUrls: ['./parishioner.component.css']
})
export class ParishionerComponent implements OnInit, OnDestroy {

  id: string;

  paramSub: Subscription;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.paramSub = this.route.params.subscribe(p => this.id = p['id']);
  }

  ngOnDestroy() {
    this.paramSub.unsubscribe();
  }

}
