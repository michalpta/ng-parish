/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ParishionerComponent } from './parishioner.component';

describe('ParishionerComponent', () => {
  let component: ParishionerComponent;
  let fixture: ComponentFixture<ParishionerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParishionerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParishionerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
