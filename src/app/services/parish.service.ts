import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/of';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { Parishioner } from '../models/parishioner';
import { Offering } from '../models/offering';

@Injectable()
export class ParishService {

  private parishioners: AngularFireList<Parishioner>;
  private offerings: AngularFireList<Offering>;

  constructor(private db: AngularFireDatabase) {
    this.parishioners = this.db.list('parishioners');
    this.offerings = this.db.list('offerings');
  }

  getParishioners(): Observable<Parishioner[]> {
    return this.parishioners.snapshotChanges()
      .map(changes => changes.map(p => ({ $key: p.payload.key, ...p.payload.val() })));
  }

  getParishioner(id: string): Observable<Parishioner> {
    return this.db.object<Parishioner>('parishioners/' + id).snapshotChanges()
      .map(p => ({ $key: p.payload.key, ...p.payload.val() }));
  }
  getParishionerOfferings(id: string): Observable<Offering[]> {
    return this.db.list<Offering>('offerings', ref => ref.orderByChild('parishioner').equalTo(id)).snapshotChanges()
      .map(o => o.map(v => ({ $key: v.payload.key, ...v.payload.val() })));
  }

  addParishioner(parishioner: Parishioner): firebase.database.ThenableReference {
    let address = [parishioner.city, parishioner.street, parishioner.streetNumber].filter(e => e).join(' ');
    parishioner.address = address;
    return this.parishioners.push(parishioner);
  }
  updateParishioner(parishioner: Parishioner): Promise<void> {
    const address = [parishioner.city, parishioner.street, parishioner.streetNumber].filter(e => e).join(' ');
    return this.db.list('parishioners').update(parishioner.$key, {
        name: parishioner.name || null,
        address: address || null,
        city: parishioner.city || null,
        street: parishioner.street || null,
        streetNumber: parishioner.streetNumber || null,
      });
  }
  deleteParishioner(parishioner: Parishioner): Promise<void> {
    return this.getParishionerOfferings(parishioner.$key)
      .first()
      .flatMap(o => o.length > 0 ? Observable.forkJoin(o.map(v => this.deleteOffering(v))) : Observable.of(null))
      .flatMap(() => this.parishioners.remove(parishioner.$key))
      .toPromise();
  }

  addOffering(parishionerId: string, amount: string, date?: Date): firebase.database.ThenableReference {
    let offering = new Offering();
    offering.amount = amount;
    offering.parishioner = parishionerId;
    offering.date = date ? date : Date.now();
    return this.offerings.push(offering);
  }
  updateOffering(offering: Offering): Promise<void> {
    return this.db.list('offerings').update(offering.$key, { amount: offering.amount, date: offering.date });
  }
  deleteOffering(offering: Offering): Promise<void> {
    return this.offerings.remove(offering.$key);
  }

  getOfferingsFromDateRange(dateStart: Date, dateEnd: Date): Observable<Offering[]> {
    return this.db.list<Offering>('offerings', ref =>
        ref.orderByChild('date').startAt(dateStart.valueOf()).endAt(dateEnd.valueOf()))
      .snapshotChanges()
      .map(o => o.map(v => ({ $key: v.payload.key, ...v.payload.val() })));
  }

  mergeParishioners(destinationParishioner: Parishioner, sourceParishioner: Parishioner): Observable<void> {
    return this.getParishionerOfferings(sourceParishioner.$key)
      .first()
      .flatMap(o => {
          if (o.length === 0) {
            return Observable.of([]);
          }
          return Observable.forkJoin(o.map(v => this.addOffering(destinationParishioner.$key, v.amount, v.date)))
            .flatMap(() => Observable.forkJoin(o.map(v => this.deleteOffering(v))))
        }
      )
      .flatMap(() => this.parishioners.remove(sourceParishioner.$key));
  }

}
