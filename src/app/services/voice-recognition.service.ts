import { Injectable, NgZone } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class VoiceRecognitionService {

  voice: any;
  resultEvent$: Subject<any> = new Subject<any>();
  listeningStatus$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private zone: NgZone) {
    if ('webkitSpeechRecognition' in window) {
      const { webkitSpeechRecognition } = (window as any);
      this.voice = new webkitSpeechRecognition();
      this.voice.lang = 'pl-pl';
      this.voice.maxAlternatives = 10;
      this.voice.onresult = event => this.zone.run(() => this.resultEvent$.next(event));
      this.voice.onaudiostart = event => this.zone.run(() => this.listeningStatus$.next(true));
      this.voice.onaudioend = event => this.zone.run(() => this.listeningStatus$.next(false));
    }
  }

  start(): void {
    if (this.voice && !this.isListening) {
      this.voice.start();
    }
  }

  stop(): void {
    if (this.voice && this.isListening) {
      this.voice.stop();
    }
  }

  toggle(): void {
    if (this.isListening) {
      this.stop();
    }
    else {
      this.start()
    }
  }

  getResults(): Observable<string> {
    return this.resultEvent$
      .asObservable()
      .do(event => console.log(event))
      .map(event => event.results[event.results.length - 1][0].transcript);
  }

  getListeningStatus(): Observable<boolean> {
    return this.listeningStatus$.asObservable();
  }

  get isListening(): boolean {
    return this.listeningStatus$.getValue();
  }

}
