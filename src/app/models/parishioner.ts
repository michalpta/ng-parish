export class Parishioner {
    name: string;
    address: string;
    city: string;
    street: string;
    streetNumber: string;
    streetNumberNumeric: string;
    id: string;

    $key: any;
    $exists: any;
}
