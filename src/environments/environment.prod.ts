export const environment = {
  production: true,

  appConfig: {
    title: "Parafia +",
    subtitle: "pw. Matki Bożej Częstochowskiej <br/> w Woli Zachariaszowskiej",
    logoFilename: "church.png",
    language: "pl"
  },

  firebaseConfig: {
    apiKey: "AIzaSyD_-74GoWmIowqQzFcW4j0zk_0OVDXWN6w",
    authDomain: "ng-parish-app.firebaseapp.com",
    databaseURL: "https://ng-parish-app.firebaseio.com",
    storageBucket: "ng-parish-app.appspot.com",
    messagingSenderId: "36517619520"
  },

  reportConfig: {
    cities: ["Garlica Duchowna", "Garliczka", "Górna Wieś", "Wola Zachariaszowska"],
    otherCities: "Inni"
  }
};
